function SaveArDroneVideo(block)

  setup(block);
  
%endfunction

function setup(block)
   
  %% Register number of input and output ports
  block.NumInputPorts  = 1;
  block.NumOutputPorts = 0;
  block.NumDialogPrms  = 1;
  


  %% Setup functional port properties to dynamically
  %% inherited.
  block.SetPreCompInpPortInfoToDynamic;
  
  %% Set block sample time to inhereted
  block.SampleTimes = [-1 0];
  
  %% Set the block simStateCompliance to default (i.e., same as a built-in block)
  block.SimStateCompliance = 'DefaultSimState';

  %% Register methods
  block.RegBlockMethod('InitializeConditions',    @InitConditions);  
  block.RegBlockMethod('Update',                  @Update);  


  %% Set block as a viewer
  block.SetSimViewingDevice(true);
  
%endfunction

function InitConditions(block)
fileName = block.DialogPrm(1).Data;
ArDroneVideo = [];
save(fileName,'ArDroneVideo');
  
%endfunction



function Update(block)


%% Get data

fileName = block.DialogPrm(1).Data;
load(fileName);
data = block.InputPort(1).Data';
ArDroneVideo = [ArDroneVideo; data];
save(fileName,'ArDroneVideo');

%endfunction


