userInput = questdlg('Are you sure you wish to overwrite the calibration data used by the AR Drone models to the sensor default specifications?','Confirm save', 'Yes', 'No','No');

switch userInput
    case 'Yes'
        %% open the dictionary
        dict = Simulink.data.dictionary.open('ArDroneDataDictionary.sldd');
        Data = getSection(dict,'Design Data');
        accOffEntry = getEntry(Data,'accelerometerXyzOffsets');
        accGainEntry = getEntry(Data,'accelerometerXyzGains');
        gyroEntry = getEntry(Data,'gyroXyzOffsets');
        magEntry = getEntry(Data,'magnetoXyzOffsets');

        %% store values in the dictionary

        setValue(accOffEntry,[2048 2048 2048]);
        setValue(accGainEntry,[512 512 512]);
        setValue(gyroEntry,[0 0 0]);
        setValue(magEntry,[0 0 0]);
        saveChanges(dict);
        setInArDict('dronePowerScaling',1);
        setInArDict('magnetoXyzSpans',[300;300;300]);
        msgbox('Saved the calibration data to the AR Drone data dictionary. All models linked to the data dictionary will use the saved calibration parameters.');
    otherwise
        msgbox('User cancelled save, data dictionary calibration parameters were not overwritten.');
end