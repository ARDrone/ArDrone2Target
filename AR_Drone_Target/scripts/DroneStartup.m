% This script defines a project shortcut. 
%
% To get a handle to the current project use the following function:
%
% project = simulinkproject();
%
% You can use the fields of project to get information about the currently 
% loaded project. 
%
% See: help simulinkproject

%% If you do not want to see the documentation at startup, set this value to 0
showDoc = 1;

% parameter to keep track of whether all parts of the startup script work
startupFailure = 0;

%% Find the base folder since different MATLAB versions execute and find files differently
% some versions will run this script from the simulink project folder,
% others from the folder it is in
currentFolder = pwd;
if isequal(currentFolder(end-6:end),'scripts') 
   cd ../.. 
end

%% Register the current path as the base project path in the data dictionary
myDictionaryObj = Simulink.data.dictionary.open('ArDroneDataDictionary.sldd');
dDataSectObj = getSection(myDictionaryObj,'Design Data');
dDataSectObj.getEntry('projectDirectory').setValue(pwd);

%% Check if the compiler has been registered

if exist(['AR_Drone_Target/registry/thirdpartytools/thirdpartytools_win32.xml'],'file') == 0 || ...
        exist(['AR_Drone_Target/registry/thirdpartytools/thirdpartytools_win64.xml'],'file') == 0
    disp('No third party compiler has been registered for the AR Drone, running install script')
    cd AR_Drone_Target/scripts/
    try
        run('install_script.m') 
    catch
        startupFailure = 1;
        disp('FAILED TO REGISTER THIRD PARTY COMPILER. Make sure you have installed Code Sourcery before opening the AR Drone 2.0 Target')
    end
    % install script resets cd to baseFolder
end

%% Check if TargetRegistry has been set
if exist(['AR_Drone_Target/registry/gcc_codesourcery_arm_linux_gnueabihf_gmake_win64_v4_8.mat'],'file') == 0
    disp('No toolchain info mat file was found for use with rtwTargetInfo.m, generating the gcc_codesourcery_arm_linux_gnueabihf_gmake_win64 mat file')
    % cd to the folder where the mat file should be put
    cd AR_Drone_Target/registry
    run('../registry/generateTargetInfoMatFile.m');  % create the mat file containing the toolchain info
    cd ../..
    RTW.TargetRegistry.getInstance('reset');         % reset the TargetRegistry such that rtwTargetInfo.m is called when a model is made for the first time
end    

%% register the compiler
sl_refresh_customizations;

%% Compile the video library if needed
if exist(['AR_Drone_Target/blocks/videolib/AR_Drone_Front_Camera.tlc'],'file') == 0 ||...
        exist(['AR_Drone_Target/blocks/videolib/AR_Drone_Bottom_Camera.tlc'],'file') == 0
    disp('No video source files were found, starting compilation of the video library using the Legacy Code Tool')
    cd AR_Drone_Target/blocks/videolib
    try
        run('Generate_AR_Drone_Video.m')
    catch
        startupFailure = 1;
        disp('FAILED TO GENERATE VIDEO DRIVER BLOCKS. Make sure you have installed a C compiler before opening the AR Drone 2.0 Target. You can install MinGW64 from the Add On Explorer in MATLAB')
    end
    bdclose; % close the video lib it opens
    cd ../../..
end

%% Create the s functions for the AR_Drone_Library blocks
if exist(['AR_Drone_Target/blocks/rtwmakecfg.m'],'file') == 0 % the rtwmakecfg.m is created at the very end of Generate_AR_Drone_S_Functions
    disp('No rtwmakecfg found for the blocks, running Generate_AR_Drone_S_Functions')
    cd AR_Drone_Target/blocks
    try
        run('Generate_AR_Drone_S_Functions.m')
    catch
        startupFailure = 1;
        disp('FAILED TO GENERATE LIBRARY BLOCKS. Make sure you have installed a C compiler before opening the AR Drone 2.0 Target. You can install MinGW64 from the Add On Explorer in MATLAB')
    end
   cd ../..
end

%% set build folder

if exist([pwd '/Build'],'dir') == 0
    disp('Creating build folder')
    mkdir(['Build'])
end
set_param(0, 'CacheFolder', ['Build']);
set_param(0, 'CodeGenFolder', ['Build']);

%% disable warning for tunable parameter structs and store current state in SLDD for restoring when closing project
origWarningState = warning('off', 'Simulink:Engine:ExtModeCannotDownloadParamBecauseNoHostToTarget');
myDictObj = Simulink.data.dictionary.open('ArDroneDataDictionary.sldd');
dDataSectObj = getSection(myDictObj,'Other Data');
if exist(dDataSectObj, 'origWarningState')
    dDataSectObj.getEntry('origWarningState').setValue(origWarningState)
else
    dDataSectObj.addEntry('origWarningState',origWarningState)
end
myDictObj.saveChanges

%% show the documentation and library

if showDoc == 1 && startupFailure == 0
%     open('AR_Drone_2_Library')
    pause(1); % Give MATLAB help system some time to recognize updated path
    % Else the doc is opened in a browser window instead of documentation
    open('Docs/html/ARDrone2Toolbox.html');
end

%% The startup failed, tell the user
if startupFailure == 1
   disp('============================ AR DRONE 2.0 STARTUP FAILED ===================================')
   disp('One or more parts of the startup script failed. Information about the failure is show above.')
   disp('Close the project and fix the issue before opening the AR Drone 2.0 Target.')
   error('Startup failed. Information about the failure is show in the Command Window. Close the project and fix the issue before opening the project again.')
   pause;
end
%% clean up
clear % this was clear all, was "all" really necessary?



